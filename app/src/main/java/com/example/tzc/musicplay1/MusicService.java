package com.example.tzc.musicplay1;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.util.Log;

public class MusicService extends Service {

    public static final String MYSERVICE = "MYSERVICE";
    private MediaPlayer mediaPlayer;

    public class MusicBinder extends Binder {
        public void play() {
            MusicService.this.play();
        }
        public void pause() {
            MusicService.this.pause();
        }
    }

    public MusicService() {
    }

    public void play() {
        mediaPlayer.start();
    }

    public void pause() {
        mediaPlayer.pause();
    }

    @Override
    public IBinder onBind(Intent intent) {

        Log.i(MYSERVICE, "onBind");
        return new MusicBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mediaPlayer = MediaPlayer.create(this, R.raw.music);
        Log.i(MYSERVICE, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(MYSERVICE, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(MYSERVICE, "onDestroy");
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(MYSERVICE, "onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
        Log.i(MYSERVICE, "onRebind");
    }
}
